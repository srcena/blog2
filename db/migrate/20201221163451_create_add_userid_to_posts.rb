class CreateAddUseridToPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :add_userid_to_posts do |t|
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
