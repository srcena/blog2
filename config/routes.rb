Rails.application.routes.draw do
  resources :add_userid_to_comments
  resources :add_userid_to_posts
  resources :users
  resources :comments
  resources :posts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
