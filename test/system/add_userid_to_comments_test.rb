require "application_system_test_case"

class AddUseridToCommentsTest < ApplicationSystemTestCase
  setup do
    @add_userid_to_comment = add_userid_to_comments(:one)
  end

  test "visiting the index" do
    visit add_userid_to_comments_url
    assert_selector "h1", text: "Add Userid To Comments"
  end

  test "creating a Add userid to comment" do
    visit add_userid_to_comments_url
    click_on "New Add Userid To Comment"

    fill_in "User", with: @add_userid_to_comment.user_id
    click_on "Create Add userid to comment"

    assert_text "Add userid to comment was successfully created"
    click_on "Back"
  end

  test "updating a Add userid to comment" do
    visit add_userid_to_comments_url
    click_on "Edit", match: :first

    fill_in "User", with: @add_userid_to_comment.user_id
    click_on "Update Add userid to comment"

    assert_text "Add userid to comment was successfully updated"
    click_on "Back"
  end

  test "destroying a Add userid to comment" do
    visit add_userid_to_comments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Add userid to comment was successfully destroyed"
  end
end
