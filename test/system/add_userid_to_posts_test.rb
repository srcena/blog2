require "application_system_test_case"

class AddUseridToPostsTest < ApplicationSystemTestCase
  setup do
    @add_userid_to_post = add_userid_to_posts(:one)
  end

  test "visiting the index" do
    visit add_userid_to_posts_url
    assert_selector "h1", text: "Add Userid To Posts"
  end

  test "creating a Add userid to post" do
    visit add_userid_to_posts_url
    click_on "New Add Userid To Post"

    fill_in "User", with: @add_userid_to_post.user_id
    click_on "Create Add userid to post"

    assert_text "Add userid to post was successfully created"
    click_on "Back"
  end

  test "updating a Add userid to post" do
    visit add_userid_to_posts_url
    click_on "Edit", match: :first

    fill_in "User", with: @add_userid_to_post.user_id
    click_on "Update Add userid to post"

    assert_text "Add userid to post was successfully updated"
    click_on "Back"
  end

  test "destroying a Add userid to post" do
    visit add_userid_to_posts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Add userid to post was successfully destroyed"
  end
end
